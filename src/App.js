import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Home from './pages/Home';
import Clientes, {ClienteDetail} from './pages/Clientes';
import Produtos, {ProdutoDetail} from './pages/Produtos';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/clientes" component={Clientes}/>
        <Route exact path="/cliente/:client_id" component={ClienteDetail}/>

        <Route exact path="/produtos" component={Produtos}/>
        <Route exact path="/produto/:produto_id" component={ProdutoDetail}/>

        <Route any component={Home}/>
      </Switch>
    </Router>
  );
}

export default App;
