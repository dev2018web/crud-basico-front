import React from 'react';
import Header from './shared/Header';
import { Jumbotron, Table, Button, Modal, Form } from 'react-bootstrap';
import request from '../configs/api';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt, faPen, faEye } from '@fortawesome/free-solid-svg-icons'
import {Link} from 'react-router-dom';

const getProduto = async (produto_id, classe) => {
    try{
        const response = await request.get(`produto/${produto_id}`)

        const {produto} = response.data;

        classe.setState({produto});
    }catch(Error){
        console.log(Error);
    }
}

export class ProdutoDetail extends React.Component{
    state = {
        produto_id: this.props.match.params.produto_id,
        produto: '',
    }

    componentDidMount(){
       const {produto_id} = this.state;

       getProduto(produto_id, this);
    }

    render(){
        const {produto} = this.state;

        return(
            <div>
                <Header/>

                <div className="container">
                    <Jumbotron>
                        <h1>{produto.nome}</h1>

                        <p>Nome: {produto.nome}</p>
                        <p>Preço: {produto.preco_compra}</p>
                        <p>Data de Publicação: {produto.data_entrada}</p>
                    </Jumbotron>
                </div>
            </div>
        )
    }
}

class Produtos extends React.Component{
    state = {
        produtos: '',
        produto: ''
    }

    componentDidMount(){
        this.getProdutos();
    }

    getProdutos = async () => {
        try{
            const response = await request.get('produto');

            const {produtos} = response.data;

            this.setState({produtos});
        }catch(Error){
            console.log(Error);
        }
    }
    
    deleteProduto = async (client_id) => {
        try{
            this.closeModal();

            await request.delete(`produto/${client_id}`);
            
            this.getProdutos();
        }catch(Error){
            console.log(Error);
        }
    }

    updateContact = async (client_id) => {
        const {produto} = this.state;
        try{
            this.closeModal();

            await request.put(`produto/${client_id}`, produto);
            
            this.getProdutos();
        }catch(Error){
            console.log(Error);
        }
    }

    createContact = async () => {
        const {produto} = this.state;
        try{
            this.closeModal();

            await request.post(`produto`, produto);
            
            this.getProdutos();
        }catch(Error){
            console.log(Error);
        }
    }

    confirmDelete = (id) => {
        getProduto(id, this);

        this.setState({visibleModalDelete: true});
    }

    closeModal = () => {
        this.setState({visibleModalDelete: false});
        this.setState({visibleModalUpdate: false});
        this.setState({visibleModalCreate: false});
    }

    opendModalUpdate = (id) => {
        getProduto(id, this);

        this.setState({visibleModalUpdate: true})
    }

    opendModalCreate = () => {
        this.setState({visibleModalCreate: true})
    }

    handleInputUpdate = (event) => {
        const {value} = event.target;
        const {name} = event.target;
        const {produto} = this.state;

        this.setState({produto: {...produto, [name]: value}});
    }

    handleInputCreate = (event) => {
        const {value} = event.target;
        const {name} = event.target;
        const {produto} = this.state;

        this.setState({produto: {...produto, [name]: value}});
    }

    render(){
        const {produtos, visibleModalDelete,visibleModalUpdate,visibleModalCreate, produto} = this.state;

        return(
            <div>
                <Header/>
                <div className="container">
                    <Jumbotron>
                        <h1>Produtos</h1>
                    </Jumbotron>

                    <Button style={{marginBottom: '20px'}} onClick={this.opendModalCreate}>Adicionar novo cliente</Button>

                    <Table striped bordered hover>
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Preço da compra</th>
                            <th>Data da entrada</th>
                            <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {(produtos) ? produtos.map(produto => (
                                <tr>
                                <td>{produto.id}</td>
                                <td>{produto.nome}</td>
                                <td>{produto.preco_compra}</td>
                                <td>{produto.data_entrada}</td>
                                <td className="row">
                                    <Link  to={`/produto/${produto.id}`}><FontAwesomeIcon style={{width: '60px'}} className="col text-primary" icon={faEye} /></Link>
                                    <FontAwesomeIcon style={{cursor: 'pointer'}} onClick={() => this.confirmDelete(produto.id)} className="col text-danger" icon={faTrashAlt} />
                                    <FontAwesomeIcon style={{cursor: 'pointer'}} onClick={() => this.opendModalUpdate(produto.id)} className="col text-success"icon={faPen} />
                                </td>
                                </tr>
                            )) : ''}
                        </tbody>
                        </Table>

                        <ModalConfirmeDelete props={{visibleModalDelete, classe: this, produto}}/>
                        <ModalUpdate props={{visibleModalUpdate, classe: this, produto}}/>
                        <ModalCreate props={{visibleModalCreate, classe: this, produto}}/>
                    </div>
            </div>
        )
    }
}

function ModalCreate(props){
    const {visibleModalCreate, classe, produto} = props.props;

    return(
        <div>
            <Modal show={visibleModalCreate} onHide={classe.closeModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Criar produto </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Nome</Form.Label>
                        <Form.Control type="text" name="nome" onChange={classe.handleInputCreate}/>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Preço do produto</Form.Label>
                        <Form.Control type="number" name="preco_compra"  onChange={classe.handleInputCreate}/>
                    </Form.Group>
                </Form>

                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={classe.closeModal}>
                    Cancelar
                </Button>
                <Button variant="success" onClick={() => classe.createContact(produto.id)}>
                    Salvar
                </Button>
                </Modal.Footer>
            </Modal>   
        </div>
    )
}

function ModalUpdate(props){
    const {visibleModalUpdate, classe, produto} = props.props;

    return(
        <div>
            <Modal show={visibleModalUpdate} onHide={classe.closeModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Atualizar produto </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Nome</Form.Label>
                        <Form.Control type="text" name="nome" value={produto.nome} onChange={classe.handleInputUpdate}/>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Preço do produto</Form.Label>
                        <Form.Control type="number" name="preco_compra"  value={produto.preco_compra} onChange={classe.handleInputUpdate}/>
                    </Form.Group>
                </Form>

                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={classe.closeModal}>
                    Cancelar
                </Button>
                <Button variant="success" onClick={() => classe.updateContact(produto.id)}>
                    Salvar
                </Button>
                </Modal.Footer>
            </Modal>   
        </div>
    )
}


function ModalConfirmeDelete(props){
    const {visibleModalDelete, classe, produto} = props.props;
    return(
        <div>
            <Modal show={visibleModalDelete} onHide={classe.closeModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Deseja deletar {produto.nome}? </Modal.Title>
                </Modal.Header>
                <Modal.Body>Você precisa confirmar para deletar esse contato.</Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={classe.closeModal}>
                    Cancelar
                </Button>
                <Button variant="danger" onClick={() => classe.deleteProduto(produto.id)}>
                    Deletar
                </Button>
                </Modal.Footer>
            </Modal>   
        </div>
    )
}




export default Produtos;