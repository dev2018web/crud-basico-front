import React from 'react';
import Header from './shared/Header';
import { Jumbotron, Table, Button, Modal, Form } from 'react-bootstrap';
import request from '../configs/api';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt, faPen, faEye } from '@fortawesome/free-solid-svg-icons'
import {Link} from 'react-router-dom';

const  getClient = async (client_id, classe) => {
    try{
        const response = await request.get(`cliente/${client_id}`)

        const {cliente} = response.data;

        classe.setState({cliente});
    }catch(Error){
        console.log(Error);
    }
}

class Clientes extends React.Component{
    state = {
        clientes: '',
        cliente: '',
        visibleModalDelete: false,
        visibleModalUpdate: false,
        visibleModalCreate: false,
    }

    componentDidMount(){
        this.getClientes();
    }

    getClientes = async () => {
        try{
            const response = await request.get('cliente');

            const {clientes} = response.data;

            this.setState({clientes});
        }catch(Error){
            console.log(Error);
        }
    }

    deleteContact = async (client_id) => {
        try{
            this.closeModal();

            const response = await request.delete(`cliente/${client_id}`)
            
            this.getClientes();
        }catch(Error){
            console.log(Error);
        }
    }

    updateContact = async (client_id) => {
        const {cliente} = this.state;
        try{
            this.closeModal();

            await request.put(`cliente/${client_id}`, cliente);
            
            this.getClientes();
        }catch(Error){
            console.log(Error);
        }
    }

    createContact = async () => {
        const {cliente} = this.state;
        try{
            this.closeModal();

            await request.post(`cliente`, cliente);
            
            this.getClientes();
        }catch(Error){
            console.log(Error);
        }
    }

    confirmDelete = (id) => {
        getClient(id, this);

        this.setState({visibleModalDelete: true});
    }

    closeModal = () => {
        this.setState({visibleModalDelete: false});
        this.setState({visibleModalUpdate: false});
        this.setState({visibleModalCreate: false});
    }

    opendModalUpdate = (id) => {
        getClient(id, this);

        this.setState({visibleModalUpdate: true})
    }

    opendModalCreate = () => {
        this.setState({visibleModalCreate: true})
    }

    handleInputUpdate = (event) => {
        const {value} = event.target;
        const {name} = event.target;
        const {cliente} = this.state;

        this.setState({cliente: {...cliente, [name]: value}});
    }

    handleInputCreate = (event) => {
        const {value} = event.target;
        const {name} = event.target;
        const {cliente} = this.state;

        this.setState({cliente: {...cliente, [name]: value}});
    }

    render(){
        const {clientes, visibleModalDelete,visibleModalUpdate,visibleModalCreate, cliente} = this.state;
        return(
            <div>
                <Header/>
                <div className="container">
                    <Jumbotron>
                        <h1>Clientes</h1>
                    </Jumbotron>

                    <Button style={{marginBottom: '20px'}} onClick={this.opendModalCreate}>Adicionar novo cliente</Button>

                    <Table striped bordered hover>
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>Endereço</th>
                            <th>Telefone</th>
                            <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {(clientes) ? clientes.map(cliente => (
                                <tr>
                                <td>{cliente.id}</td>
                                <td>{cliente.nome}</td>
                                <td>{cliente.cpf}</td>
                                <td>{cliente.endereco}</td>
                                <td>{cliente.telefone}</td>
                                <td className="row">
                                    <Link  to={`/cliente/${cliente.id}`}><FontAwesomeIcon style={{width: '60px'}} className="col text-primary" icon={faEye} /></Link>
                                    <FontAwesomeIcon style={{cursor: 'pointer'}} onClick={() => this.confirmDelete(cliente.id)} className="col text-danger" icon={faTrashAlt} />
                                    <FontAwesomeIcon style={{cursor: 'pointer'}} onClick={() => this.opendModalUpdate(cliente.id)} className="col text-success"icon={faPen} />
                                </td>
                                </tr>
                            )) : ''}
                        </tbody>
                        </Table>
                    </div>


                    <ModalConfirmeDelete props={{visibleModalDelete, classe: this, cliente}}/>
                    <ModalUpdate props={{visibleModalUpdate, classe: this, cliente}}/>
                    <ModalCreate props={{visibleModalCreate, classe: this, cliente}}/>
            </div>
        )
    }
}

function ModalCreate(props){
    const {visibleModalCreate, classe, cliente} = props.props;

    return(
        <div>
            <Modal show={visibleModalCreate} onHide={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Criar Cliente </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Nome</Form.Label>
                        <Form.Control type="text" name="nome" onChange={classe.handleInputCreate}/>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>CPF</Form.Label>
                        <Form.Control type="text" name="cpf"  onChange={classe.handleInputCreate}/>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Endereço</Form.Label>
                        <Form.Control type="text" name="endereco"  onChange={classe.handleInputCreate}/>
                    </Form.Group>


                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Telefone</Form.Label>
                        <Form.Control type="text" name="telefone"  onChange={classe.handleInputCreate}/>
                    </Form.Group>

                </Form>

                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={classe.closeModal}>
                    Cancelar
                </Button>
                <Button variant="success" onClick={() => classe.createContact(cliente.id)}>
                    Salvar
                </Button>
                </Modal.Footer>
            </Modal>   
        </div>
    )
}

function ModalUpdate(props){
    const {visibleModalUpdate, classe, cliente} = props.props;

    return(
        <div>
            <Modal show={visibleModalUpdate} onHide={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Atualizar Cliente </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Nome</Form.Label>
                        <Form.Control type="text" name="nome" value={cliente.nome} onChange={classe.handleInputUpdate}/>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>CPF</Form.Label>
                        <Form.Control type="text" name="cpf"  value={cliente.cpf} onChange={classe.handleInputUpdate}/>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Endereço</Form.Label>
                        <Form.Control type="text" name="endereco"  value={cliente.endereco} onChange={classe.handleInputUpdate}/>
                    </Form.Group>


                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Telefone</Form.Label>
                        <Form.Control type="text" name="telefone" value={cliente.telefone} onChange={classe.handleInputUpdate}/>
                    </Form.Group>

                </Form>

                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={classe.closeModal}>
                    Cancelar
                </Button>
                <Button variant="success" onClick={() => classe.updateContact(cliente.id)}>
                    Salvar
                </Button>
                </Modal.Footer>
            </Modal>   
        </div>
    )
}


function ModalConfirmeDelete(props){
    const {visibleModalDelete, classe, cliente} = props.props;
    return(
        <div>
            <Modal show={visibleModalDelete} onHide={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Deseja deletar {cliente.nome}? </Modal.Title>
                </Modal.Header>
                <Modal.Body>Você precisa confirmar para deletar esse contato.</Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={classe.closeModal}>
                    Cancelar
                </Button>
                <Button variant="danger" onClick={() => classe.deleteContact(cliente.id)}>
                    Deletar
                </Button>
                </Modal.Footer>
            </Modal>   
        </div>
    )
}


export class ClienteDetail extends React.Component{
    state = {
        client_id: this.props.match.params.client_id,
        cliente: '',
    }

    componentDidMount(){
       const {client_id} = this.state;

       getClient(client_id, this);
    }

   
    render(){
        const {cliente} = this.state;

        return(
            <div>
                <Header/>

                <div className="container">
                    <Jumbotron>
                        <h1>{cliente.nome}</h1>

                        <p>Nome: {cliente.nome}</p>
                        <p>CPF: {cliente.cpf}</p>
                        <p>Endereço: {cliente.endereco}</p>
                        <p>Telefone: {cliente.telefone}</p>
                    </Jumbotron>
                </div>
            </div>
        )
    }
}

export default Clientes;