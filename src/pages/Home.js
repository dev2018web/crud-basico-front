import React from 'react';
import Header from './shared/Header';
import { Jumbotron } from 'react-bootstrap';
import request from '../configs/api';

class Home extends React.Component{
    render(){
        return(
            <div>
                <Header/>
                    <Jumbotron className="container">
                    <h1>Seja bem-vindo!</h1>
                    <p>
                        Você está no mais belo e melhor sistema que existe: o sistema de listagem de clientes e produtos
                    </p>
                    <p>
                    </p>
                </Jumbotron>
            </div>
        )
    }
}

export default Home;