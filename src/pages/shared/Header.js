import React from 'react';
import { Nav } from 'react-bootstrap';
import { Link } from "react-router-dom";

class Header extends React.Component {

    render(){
        return(
            <Nav className="bg-dark"
  activeKey="/home"
//   onSelect={selectedKey => alert(`selected ${selectedKey}`)}
>
  <Nav.Item>
    <Nav.Link><Link to="/home">Home</Link></Nav.Link>
  </Nav.Item>
  <Nav.Item>
    <Nav.Link><Link to="/clientes">Clientes</Link></Nav.Link>
  </Nav.Item>
  <Nav.Item>
    <Nav.Link><Link to="/produtos">Produtos</Link></Nav.Link>
  </Nav.Item>
</Nav>
        )
    }
}


export default Header;
